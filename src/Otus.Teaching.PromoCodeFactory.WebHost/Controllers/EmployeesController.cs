﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult> AddEmployeeAsync(EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest();

            var roles = new List<Role>();
            foreach (var role in employeeRequest.Roles)
            {
                roles.Add(await _rolesRepository.GetByIdAsync(role));
            }

            var employee = new Employee()
            {
                Id = employeeRequest.Id,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Roles = roles
            };

            var response = await _employeeRepository.AddAsync(employee);

            return Ok(response);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var response = await _employeeRepository.DeleteByIdAsync(id);

            if (response)
                return Ok();
            else
                return NotFound();
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPatch()]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(employeeRequest.Id);

            if (employee == null)
                return NotFound();

            var roles = new List<Role>();
            foreach (var role in employeeRequest.Roles)
            {
                roles.Add(await _rolesRepository.GetByIdAsync(role));
            }

            employee.FirstName = employeeRequest.FirstName;
            employee.LastName = employeeRequest.LastName;
            employee.Email = employeeRequest.Email;
            employee.AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount;
            employee.Roles = roles;

            await _employeeRepository.UpdateAsync(employee);

            return Ok();
        }
    }
}