﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(Data.Last().Id);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            return Task.FromResult(Data.Remove(Data.FirstOrDefault(x => x.Id == id)));
        }

        public Task<bool> UpdateAsync(T item)
        {
            var oldItem = Data.FirstOrDefault(x => x.Id == item.Id);
            if (oldItem is null)
                return Task.FromResult(false);
            oldItem = item;

            return Task.FromResult(true);
        }
    }
}